module github.com/ibm-messaging/mq-container-mft

go 1.16

require (
	github.com/Jeffail/gabs v1.4.0
	github.com/antchfx/xmlquery v1.3.9
	github.com/ibm-messaging/mq-container v9.1.3+incompatible
	github.com/ibm-messaging/mq-golang v3.0.0+incompatible
	github.com/icza/backscanner v0.0.0-20210726202459-ac2ffc679f94
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/client_model v0.2.0
	github.com/spf13/pflag v1.0.5
	github.com/syndtr/gocapability v0.0.0-20200815063812-42c35b437635
	github.com/tidwall/gjson v1.12.1
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	golang.org/x/net v0.0.0-20211209124913-491a49abca63 // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486
	k8s.io/apimachinery v0.23.0
	software.sslmate.com/src/go-pkcs12 v0.0.0-20210415151418-c5206de65a78
)
